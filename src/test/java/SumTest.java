import junit.framework.TestCase;
import org.calculator.com.Sum;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by ArindamN on 16/11/2016.
 */

@RunWith(JUnit4.class)
public class SumTest extends TestCase {

    Sum calculator;

    @Before
    public void beforeInit(){ calculator = new Sum(); }

    @Test
    public void CalculateSum_test(){
        int return_value = calculator.CalculateSum(1,1);
        assertEquals(2, return_value);
    }

    @After
    public void after() { calculator = null ;}
}
