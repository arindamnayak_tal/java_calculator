package org.calculator.com;

/**
 * Created by ArindamN on 16/11/2016.
 */
public class Main {
    public static void main(String[] args) {
        int n1 = Integer.valueOf(args[0]);
        int n2 = Integer.valueOf(args[1]);
        Sum sum = new Sum();
        System.out.println("org.calculator.com.Sum = " + sum.CalculateSum(n1,n2));
    }
}
